# TRIP

## Catalog:

1. [About Project](#about-project)
2. [Project Development](#project-development)
3. [Run Project](#run-project)

## About Project

This project is made in React.js. It provides users the graphical representation of possitive and negative delays. It also shows Top 3 Source locations and Top 3 destination locations.

## Project Development

This project uses react, react-d3-components, react-dom, react-scripts and request.
The API used for retreiving data is https://asia-east2-greentoad-bfbb7.cloudfunctions.net/apiIndia/api/angular
If you want to build this project from scratch follow these steps:

1. Open terminal and run

```
create-react-app app-name
```

2. Install request and react-d3-components modules

```
npm install react-d3-components
npm install request
```

3. Create a components directory and create React components which you want. This project contains Table, Delay, Top Sources and Top Destination modules. For top destination, top sources and table component, the api data is added by app.js file but in delay component api is called.

## Run Project

Clone this repo on your local device and move to Trip directory and run

```
npm start
```
