import React, { useState, useEffect } from "react";
import "./App.css";
import callApi from "./api";
import DelayChart from "./components/delay";
import TopSource from "./components/topSource";
import TopDestination from "./components/topDestination";

function App() {
  // declaring states to store api response, top sources and top destinations
  let [apiResponse, setResponse] = useState({});
  let [topSrc1, setTopSrc1] = useState("");
  let [topSrc2, setTopSrc2] = useState("");
  let [topSrc3, setTopSrc3] = useState("");
  let [topDes1, setTopDes1] = useState("");
  let [topDes2, setTopDes2] = useState("");
  let [topDes3, setTopDes3] = useState("");
  let finalData;
  let response;
  let [chartData, setData] = useState({
    labels: [
      "Jan",
      "Feb",
      "Mar",
      "Apr",
      "May",
      "Jun",
      "Jul",
      "Aug",
      "Sep",
      "Oct",
      "Nov",
      "Dec"
    ],
    datasets: [
      {
        label: "Possitive delays",
        data: [10, 10, 10, 10, 1, 10, 10, 10, 10, 10, 10, 10],
        backgroundColor: [
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)",
          "rgb(100, 100, 100)"
        ]
      },
      {
        label: "Negative delays",
        data: [10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10],
        backgroundColor: [
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)",
          "rgb(100, 100, 10)"
        ]
      }
    ]
  });

  // function to call api and set all the states
  const fetchData = async () => {
    response = await callApi(
      "https://asia-east2-greentoad-bfbb7.cloudfunctions.net/apiIndia/api/angular"
    );
    // setting possitive and negative delay count to zero
    var poss = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    var neg = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    // counting possitive and negative delays according to month
    response.map(resp => {
      if (resp.delay > 0) {
        var dt = new Date(resp.trip_start_time);
        let index = dt.getMonth();
        poss[index] = poss[index] + 1;
      } else if (resp.delay <= 0 || resp.delay === null) {
        var dt = new Date(resp.trip_start_time);
        let index = dt.getMonth();
        neg[index] = neg[index] + 1;
      }
    });

    // update bar graph data
    setData(
      (chartData = {
        labels: [
          "Jan",
          "Feb",
          "Mar",
          "Apr",
          "May",
          "Jun",
          "Jul",
          "Aug",
          "Sep",
          "Oct",
          "Nov",
          "Dec"
        ],
        datasets: [
          {
            label: "Possitive delays",
            data: [
              poss[0],
              poss[1],
              poss[2],
              poss[3],
              poss[4],
              poss[5],
              poss[6],
              poss[7],
              poss[8],
              poss[9],
              poss[10],
              poss[11]
            ],
            backgroundColor: [
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)",
              "rgb(100, 100, 100)"
            ]
          },
          {
            label: "Negative delays",
            data: [
              neg[0],
              neg[1],
              neg[2],
              neg[3],
              neg[4],
              neg[5],
              neg[6],
              neg[7],
              neg[8],
              neg[9],
              neg[10],
              neg[11]
            ],
            backgroundColor: [
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)",
              "rgb(100, 100, 10)"
            ]
          }
        ]
      })
    );

    apiResponse = response;

    //defining variables
    let arr1 = [],
      arr2 = [],
      arr3 = [],
      arr4 = [];

    // iterating through api response array
    apiResponse.map(resp => {
      // creating hasmap for source by using parallel arrays
      let flag1 = 0;
      for (let i = 0; i < arr1.length; i++) {
        if (arr1[i] == resp.srcname) {
          arr2[i] = arr2[i] + 1;
          flag1 = 1;
        }
      }
      if (flag1 == 0) {
        arr1.push(resp.srcname);
        arr2.push(1);
      }

      // creating hasmap for destination by using parallel arrays
      let flag2 = 0;
      for (let i = 0; i < arr3.length; i++) {
        if (arr3[i] == resp.destname) {
          arr4[i] = arr4[i] + 1;
          flag2 = 1;
        }
      }
      if (flag2 == 0) {
        arr3.push(resp.destname);
        arr4.push(1);
      }
    });

    // searching top 3 sources
    let firstSrc = 0,
      secondSrc = 0,
      thirdSrc = 0;
    for (let i = 0; i < arr2.length; i++) {
      if (arr2[i] >= firstSrc) {
        topSrc3 = topSrc2;
        topSrc2 = topSrc1;
        topSrc1 = arr1[i];
        thirdSrc = secondSrc;
        secondSrc = firstSrc;
        firstSrc = arr2[i];
      } else if (arr2[i] < firstSrc) {
        if (arr2[i] >= secondSrc) {
          topSrc3 = topSrc2;
          topSrc2 = arr1[i];
          thirdSrc = secondSrc;
          secondSrc = arr2[i];
        } else {
          if (arr2[i] >= thirdSrc) {
            topSrc3 = arr1[i];
            thirdSrc = arr2[i];
          }
        }
      }
    }

    //searching top 3 destinations
    let firstDest = 0,
      secondDest = 0,
      thirdDest = 0;
    for (let i = 0; i < arr4.length; i++) {
      if (arr4[i] >= firstDest) {
        topDes3 = topDes2;
        topDes2 = topDes1;
        topDes1 = arr3[i];
        thirdDest = secondDest;
        secondDest = firstDest;
        firstDest = arr4[i];
      } else if (arr4[i] < firstDest) {
        if (arr4[i] >= secondDest) {
          topDes3 = topDes2;
          topDes2 = arr3[i];
          thirdDest = secondDest;
          secondDest = arr4[i];
        } else {
          if (arr4[i] >= thirdDest) {
            topDes3 = arr3[i];
            thirdDest = arr4[i];
          }
        }
      }
    }

    // passing top 3 sources and destination values by using DOM
    document.getElementById("Source1").innerText =
      document.getElementById("Source1").innerText + " " + topSrc1;
    document.getElementById("Source2").innerText =
      document.getElementById("Source2").innerText + " " + topSrc2;
    document.getElementById("Source3").innerText =
      document.getElementById("Source3").innerText + " " + topSrc3;
    document.getElementById("Destination1").innerText =
      document.getElementById("Destination1").innerText + " " + topDes1;
    document.getElementById("Destination2").innerText =
      document.getElementById("Destination2").innerText + " " + topDes2;
    document.getElementById("Destination3").innerText =
      document.getElementById("Destination3").innerText + " " + topDes3;
  };

  // using useEffect to update frontend when states are updated
  useEffect(() => {
    let unmounted = false;

    if (!unmounted) {
      fetchData();
    }
    return () => {
      unmounted = true;
    };
  }, []);

  return (
    <div className="App">
      <DelayChart chartData={chartData} />
      <TopSource />
      <TopDestination />
    </div>
  );
}

export default App;
