import React from "react";
import "../App.css";

// create table module for top source and top destinations
const Table = props => {
  return (
    <table className="tables">
      <thead>
        <tr>
          <th>{props.head}</th>
        </tr>
      </thead>
      <tbody className="tableBody">
        <tr>
          <td id={props.id1}>{props.id1}: </td>
        </tr>
        <tr>
          <td id={props.id2}>{props.id2}: </td>
        </tr>
        <tr>
          <td id={props.id3}>{props.id3}: </td>
        </tr>
      </tbody>
    </table>
  );
};

export default Table;
