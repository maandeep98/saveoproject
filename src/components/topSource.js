import React from "react";
import Table from "./table";

// module for top 3 sources
const TopSource = props => {
  return (
    <div className="tabs">
      <Table id1="Source1" id2="Source2" id3="Source3" head="Top Sources" />
    </div>
  );
};

export default TopSource;
