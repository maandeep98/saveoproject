import React, { useEffect, useState } from "react";
import { Bar } from "react-chartjs-2";

// Create Bar chart
const DelayChart = props => {
  return (
    <div id="delaychart">
      <Bar
        data={props.chartData}
        options={{
          scales: {
            yAxes: [
              { ticks: { beginAtZero: true, stepSize: 10 }, stacked: true }
            ],
            xAxes: [{ stacked: true }]
          },
          title: { display: true, text: "Trip Data" },
          legend: { display: true, position: "right" },
          maintainAspectRatio: false
        }}
      />
    </div>
  );
};

export default DelayChart;
