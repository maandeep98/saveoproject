import React from "react";
import Table from "./table";

// module for top 3 destinations
const TopDestination = props => {
  return (
    <div className="tabs">
      <Table
        id1="Destination1"
        id2="Destination2"
        id3="Destination3"
        head="Top Destinations"
      />
    </div>
  );
};

export default TopDestination;
